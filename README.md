#HashcatEnigma
### 介绍
HashcatEnigma是一个将Hashcat（包括OclHashcat和CudaHashcat）分布式的第三方平台。
项目使用C#语言编写（主要还是适应大量的Windows小白用户）。

### 愿景
项目的第一步是将Hashcat作为内核完全分布式化。然而我们不仅仅只想做Hashcat的分布式，随着平台成熟，其他运算内核也可以加入进来。

### 阶段
目前项目已经基本完成编写，随着服务器的到位准备铺开内测。